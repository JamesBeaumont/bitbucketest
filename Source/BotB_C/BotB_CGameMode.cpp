// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "BotB_CGameMode.h"
#include "BotB_CHUD.h"
#include "BotB_CCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABotB_CGameMode::ABotB_CGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	//DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ABotB_CHUD::StaticClass();
}
