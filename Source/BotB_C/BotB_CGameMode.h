// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BotB_CGameMode.generated.h"

UCLASS(minimalapi)
class ABotB_CGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABotB_CGameMode();
};



