// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOTB_C_BotB_CHUD_generated_h
#error "BotB_CHUD.generated.h already included, missing '#pragma once' in BotB_CHUD.h"
#endif
#define BOTB_C_BotB_CHUD_generated_h

#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_RPC_WRAPPERS
#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABotB_CHUD(); \
	friend BOTB_C_API class UClass* Z_Construct_UClass_ABotB_CHUD(); \
public: \
	DECLARE_CLASS(ABotB_CHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/BotB_C"), NO_API) \
	DECLARE_SERIALIZER(ABotB_CHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABotB_CHUD(); \
	friend BOTB_C_API class UClass* Z_Construct_UClass_ABotB_CHUD(); \
public: \
	DECLARE_CLASS(ABotB_CHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/BotB_C"), NO_API) \
	DECLARE_SERIALIZER(ABotB_CHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABotB_CHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABotB_CHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotB_CHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotB_CHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotB_CHUD(ABotB_CHUD&&); \
	NO_API ABotB_CHUD(const ABotB_CHUD&); \
public:


#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotB_CHUD(ABotB_CHUD&&); \
	NO_API ABotB_CHUD(const ABotB_CHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotB_CHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotB_CHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABotB_CHUD)


#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define BotB_C_Source_BotB_C_BotB_CHUD_h_9_PROLOG
#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BotB_C_Source_BotB_C_BotB_CHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	BotB_C_Source_BotB_C_BotB_CHUD_h_12_RPC_WRAPPERS \
	BotB_C_Source_BotB_C_BotB_CHUD_h_12_INCLASS \
	BotB_C_Source_BotB_C_BotB_CHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BotB_C_Source_BotB_C_BotB_CHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BotB_C_Source_BotB_C_BotB_CHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	BotB_C_Source_BotB_C_BotB_CHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	BotB_C_Source_BotB_C_BotB_CHUD_h_12_INCLASS_NO_PURE_DECLS \
	BotB_C_Source_BotB_C_BotB_CHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BotB_C_Source_BotB_C_BotB_CHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
