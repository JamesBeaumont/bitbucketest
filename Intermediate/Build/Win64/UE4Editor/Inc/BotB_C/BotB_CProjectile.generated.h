// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef BOTB_C_BotB_CProjectile_generated_h
#error "BotB_CProjectile.generated.h already included, missing '#pragma once' in BotB_CProjectile.h"
#endif
#define BOTB_C_BotB_CProjectile_generated_h

#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABotB_CProjectile(); \
	friend BOTB_C_API class UClass* Z_Construct_UClass_ABotB_CProjectile(); \
public: \
	DECLARE_CLASS(ABotB_CProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BotB_C"), NO_API) \
	DECLARE_SERIALIZER(ABotB_CProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABotB_CProjectile(); \
	friend BOTB_C_API class UClass* Z_Construct_UClass_ABotB_CProjectile(); \
public: \
	DECLARE_CLASS(ABotB_CProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BotB_C"), NO_API) \
	DECLARE_SERIALIZER(ABotB_CProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABotB_CProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABotB_CProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotB_CProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotB_CProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotB_CProjectile(ABotB_CProjectile&&); \
	NO_API ABotB_CProjectile(const ABotB_CProjectile&); \
public:


#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotB_CProjectile(ABotB_CProjectile&&); \
	NO_API ABotB_CProjectile(const ABotB_CProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotB_CProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotB_CProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABotB_CProjectile)


#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ABotB_CProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ABotB_CProjectile, ProjectileMovement); }


#define BotB_C_Source_BotB_C_BotB_CProjectile_h_9_PROLOG
#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BotB_C_Source_BotB_C_BotB_CProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	BotB_C_Source_BotB_C_BotB_CProjectile_h_12_RPC_WRAPPERS \
	BotB_C_Source_BotB_C_BotB_CProjectile_h_12_INCLASS \
	BotB_C_Source_BotB_C_BotB_CProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BotB_C_Source_BotB_C_BotB_CProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BotB_C_Source_BotB_C_BotB_CProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	BotB_C_Source_BotB_C_BotB_CProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	BotB_C_Source_BotB_C_BotB_CProjectile_h_12_INCLASS_NO_PURE_DECLS \
	BotB_C_Source_BotB_C_BotB_CProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BotB_C_Source_BotB_C_BotB_CProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
