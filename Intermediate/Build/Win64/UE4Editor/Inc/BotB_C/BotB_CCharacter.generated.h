// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOTB_C_BotB_CCharacter_generated_h
#error "BotB_CCharacter.generated.h already included, missing '#pragma once' in BotB_CCharacter.h"
#endif
#define BOTB_C_BotB_CCharacter_generated_h

#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_RPC_WRAPPERS
#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABotB_CCharacter(); \
	friend BOTB_C_API class UClass* Z_Construct_UClass_ABotB_CCharacter(); \
public: \
	DECLARE_CLASS(ABotB_CCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BotB_C"), NO_API) \
	DECLARE_SERIALIZER(ABotB_CCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABotB_CCharacter(); \
	friend BOTB_C_API class UClass* Z_Construct_UClass_ABotB_CCharacter(); \
public: \
	DECLARE_CLASS(ABotB_CCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BotB_C"), NO_API) \
	DECLARE_SERIALIZER(ABotB_CCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABotB_CCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABotB_CCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotB_CCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotB_CCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotB_CCharacter(ABotB_CCharacter&&); \
	NO_API ABotB_CCharacter(const ABotB_CCharacter&); \
public:


#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABotB_CCharacter(ABotB_CCharacter&&); \
	NO_API ABotB_CCharacter(const ABotB_CCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABotB_CCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABotB_CCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABotB_CCharacter)


#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ABotB_CCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ABotB_CCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ABotB_CCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ABotB_CCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ABotB_CCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ABotB_CCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ABotB_CCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ABotB_CCharacter, L_MotionController); }


#define BotB_C_Source_BotB_C_BotB_CCharacter_h_11_PROLOG
#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BotB_C_Source_BotB_C_BotB_CCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	BotB_C_Source_BotB_C_BotB_CCharacter_h_14_RPC_WRAPPERS \
	BotB_C_Source_BotB_C_BotB_CCharacter_h_14_INCLASS \
	BotB_C_Source_BotB_C_BotB_CCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BotB_C_Source_BotB_C_BotB_CCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BotB_C_Source_BotB_C_BotB_CCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	BotB_C_Source_BotB_C_BotB_CCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	BotB_C_Source_BotB_C_BotB_CCharacter_h_14_INCLASS_NO_PURE_DECLS \
	BotB_C_Source_BotB_C_BotB_CCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BotB_C_Source_BotB_C_BotB_CCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
